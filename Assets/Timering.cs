﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timering : MonoBehaviour
{
    float timeLeft = 60.0f;
    float timeDeath = 5.0f;
    public Text timer;
    public Text scoreDead;
    public bool estaMorto = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void YouDied(){
        scoreDead.text = "VOCÊ MORREU! PRESSIONE ENTER";
        Time.timeScale = 0;
        estaMorto = true;
    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;
        timer.text = timeLeft.ToString("0.00");
         if(timeLeft < 0)
         {
            YouDied();
         }

          if(Input.GetKeyDown(KeyCode.Return)){
                if(estaMorto){
			        Time.timeScale = 1;
                    SceneManager.LoadScene("menu");
                }
            }
        
    }
}
