﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageFire : MonoBehaviour
{
    public SimpleHealthBar healthBar;
    public int currentHealth = 100;

    // Start is called before the first frame update
    void Start()
    {
    //  healthBar.UpdateBar( 100, 100 );
    }

    public void TakeDamage(int damage){
        currentHealth = currentHealth - damage;
        healthBar.UpdateBar(currentHealth, 100 ); 

        if(currentHealth == 0){
            YouDied();
        }
    }

    void HealHealth(int heal){
        currentHealth = currentHealth + heal;
        if (currentHealth == 100){
            healthBar.UpdateBar(100, 100 );
        }
        healthBar.UpdateBar(currentHealth, 100 ); 
    }

    void YouDied(){
        Debug.Log("Você morreu cretino insolente");
    }

    public void OnCollisionEnter(Collision collision)
    {  
        
        if (collision.gameObject.CompareTag("Player")) {
                TakeDamage(10);
                Debug.Log("estou aqui");
        }
    }
    // Update is called once per frame
    void Update()
    {   
        // currentHealth = currentHealth - 1;
        // healthBar.UpdateBar( currentHealth, 100 );
    }
}
