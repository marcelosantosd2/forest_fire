﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthAndDamage : MonoBehaviour
{

    public SimpleHealthBar healthBar;
    public int currentHealth = 100;
    float timeDeath = 5.0f;
    public int pointScore = 0;
    public Text scoreDead;
    public bool estaMorto = false;
    Color green = new Color(0, 1, 0, 1);
    Color red = new Color (1, 0, 0, 1);
    Color orange = new Color(1, 1, 0, 1);

    // Start is called before the first frame update
    void Start()
    {

    }

    public void TakeDamage(int damage){
        currentHealth = currentHealth - damage;
        healthBar.UpdateBar(currentHealth, 100 ); 

        if(currentHealth < 70){
           healthBar.UpdateColor(orange);
        }

        if(currentHealth < 30){
           healthBar.UpdateColor(red);
        }

        if(currentHealth <= 0){
            Cursor.visible = true;
            YouDied();
        }
    }

    void HealHealth(int heal){
        currentHealth = currentHealth + heal;
        if (currentHealth == 100){
            healthBar.UpdateBar(100, 100 );
        }
        healthBar.UpdateBar(currentHealth, 100 ); 

        if(currentHealth > 30){
           healthBar.UpdateColor(orange);
        }

        if(currentHealth > 70){
           healthBar.UpdateColor(green);
        }
    }

    void YouDied(){
        scoreDead.text = "VOCÊ MORREU!  PRESSIONE ENTER";
        Time.timeScale = 0;
        estaMorto = true;
    }

    public void OnCollisionStay(Collision collision)
    {  
        if (collision.gameObject.CompareTag("Fire")) {

                
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    Destroy(collision.gameObject);
                }

                TakeDamage(1);
        }

        if (collision.gameObject.CompareTag("lixo")) {

                
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    Destroy(collision.gameObject);
                }
        }

        if (collision.gameObject.CompareTag("CuraVida")) {
                HealHealth(15);
                Destroy(collision.gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {   
        	if(Input.GetKeyDown(KeyCode.Return)){
                if(estaMorto){
			        Time.timeScale = 1;
                    SceneManager.LoadScene("menu", LoadSceneMode.Single);                }
            }
    }
}
