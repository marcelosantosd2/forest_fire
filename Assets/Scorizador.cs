﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorizador : MonoBehaviour
{
    int pointScore = 0;
    public Text score;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void UpdateScore(int points, bool type){
        if(type){
            pointScore = pointScore + points;
        } else {
            pointScore = pointScore - points;
        }
     score.text = pointScore.ToString();
    }

     public void OnCollisionStay(Collision collision)
    {  
        if (collision.gameObject.CompareTag("Fire")) {                
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    UpdateScore(6, true);
                } 
        }

        if (collision.gameObject.CompareTag("lixo")) {                
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    UpdateScore(4, true);
                } 
        }


        if (collision.gameObject.CompareTag("CuraVida")) {
                if (pointScore == 0){
                    pointScore = 0;
                } else {
                    UpdateScore(3, false);
                }
        }
    }
}
